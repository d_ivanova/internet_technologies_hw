import React, { memo } from 'react';
import { connect } from 'react-redux';
import {
	editRekvest as editRekvestServer,
	moveRekvest as moveRekvestServer,
	removeRekvest as removeRekvestServer
} from '../../models/AppModel';
import {
	editRekvestAction,
	moveRekvestBackAction,
	moveRekvestForwardAction,
	removeRekvestAction
} from '../../store/action';

const Rekvest = ({
	rekvestName,
	rekvestId,
	testerId,
	editRekvestDispatch,
	moveRekvestBackDispatch,
	moveRekvestForwardDispatch,
	removeRekvestDispatch
}) => {
	const editRekvest = async () => {
		let newRekvestName = prompt('Введите новую заявку на тестирование', rekvestName);

		if (!newRekvestName) return;

		newRekvestName = newRekvestName.trim();

		if (!newRekvestName || newRekvestName === rekvestName) return;

		const info = await editRekvestServer({ testerId, rekvestId, newRekvestName });
		console.log(info);

		editRekvestDispatch({ testerId, rekvestId, newRekvestName });
	};

	const removeRekvest = async () => {
		// eslint-disable-next-line no-restricted-globals
		if (confirm(`Заявка на тест '${rekvestName}' будет удалена. Продолжить?`)) {
			const info = await removeRekvestServer({ testerId, rekvestId });
			console.log(info);

			removeRekvestDispatch({ testerId, rekvestId });
		}
	};

	const moveRekvestBack = async () => {
		try {
			const info = await moveRekvestServer({
				testerId,
				rekvestId,
				destTesterId: testerId - 1
			});
			console.log(info);

			moveRekvestBackDispatch({ testerId, rekvestId });
		} catch (error) {
			console.log(error);
		}
	};

	const moveRekvestForward = async () => {
		try {
			const info = await moveRekvestServer({
				testerId,
				rekvestId,
				destTesterId: testerId + 1
			});
			console.log(info);

			moveRekvestForwardDispatch({ testerId, rekvestId });
		} catch (error) {
			console.log(error);
		}
	};

	return(
		<div className="ra-tester-rekvest">
			<div className="ra-tester-rekvest-text">
				{rekvestName}
			</div>
			<div className="ra-tester-rekvest-controls">
				<div className="ra-tester-rekvest-controls-row">
					<div 
						className="ra-tester-rekvest-controls-icon left-arrow-icon"
						onClick = {moveRekvestBack}
					></div>
					<div
						className="ra-tester-rekvest-controls-icon right-arrow-icon"
						onClick = {moveRekvestForward}
					></div>
				</div>
				<div className="ra-tester-rekvest-controls-row">
					<div
						className="ra-tester-rekvest-controls-icon edit-icon"
						onClick = {editRekvest}
					></div>
					<div
						className="ra-tester-rekvest-controls-icon delete-icon"
						onClick = {removeRekvest}
					></div>
				</div>
			</div>
		</div>
	);
};

const mapDispatchToProps = dispatch => ({
	editRekvestDispatch: ({ testerId, rekvestId, newRekvestName }) =>
		dispatch(editRekvestAction({ testerId, rekvestId, newRekvestName })),
	moveRekvestBackDispatch: ({ testerId, rekvestId}) =>
		dispatch(moveRekvestBackAction({ testerId, rekvestId})),
	moveRekvestForwardDispatch: ({ testerId, rekvestId}) =>
		dispatch(moveRekvestForwardAction({ testerId, rekvestId})),
	removeRekvestDispatch: ({ testerId, rekvestId}) =>
		dispatch(removeRekvestAction({ testerId, rekvestId}))
});

export default connect(
	null,
	mapDispatchToProps
)(memo(Rekvest))
