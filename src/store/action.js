const DOWNLOAD_TESTERS = 'DOWNLOAD_TESTERS';
const ADD_TESTER = 'ADD_TESTER';
const ADD_REKVEST = 'ADD_REKVEST';
const EDIT_REKVEST = 'EDIT_REKVEST';
const MOVE_REKVEST_BACK = 'MOVE_REKVEST_BACK';
const MOVE_REKVEST_FORWARD = 'MOVE_REKVEST_FORWARD';
const REMOVE_REKVEST = 'REMOVE_REKVEST';

const downloadTestersAction = (testers) => ({
	type: DOWNLOAD_TESTERS,
	payload: testers
});

const addTesterAction = (testerName) => ({
	type: ADD_TESTER,
	payload: testerName
});

const addRekvestAction = ({ rekvestName, testerId }) => ({
	type: ADD_REKVEST,
	payload: {
		testerId,
		rekvestName
	}
});

const editRekvestAction = ({ testerId, rekvestId, newRekvestName }) => ({
	type: EDIT_REKVEST,
	payload: {
		testerId,
		rekvestId,
		newRekvestName
	}
});

const moveRekvestBackAction = ({ testerId, rekvestId}) => ({
	type: MOVE_REKVEST_BACK,
	payload: {
		testerId,
		rekvestId,
	}
});

const moveRekvestForwardAction = ({ testerId, rekvestId}) => ({
	type: MOVE_REKVEST_FORWARD,
	payload: {
		testerId,
		rekvestId,
	}
});

const removeRekvestAction = ({ testerId, rekvestId}) => ({
	type: REMOVE_REKVEST,
	payload: {
		testerId,
		rekvestId,
	}
});

export {
	DOWNLOAD_TESTERS,
	ADD_TESTER,
	ADD_REKVEST,
	EDIT_REKVEST,
	MOVE_REKVEST_BACK,
	MOVE_REKVEST_FORWARD,
	REMOVE_REKVEST,
	downloadTestersAction,
	addTesterAction,
	addRekvestAction,
	editRekvestAction,
	moveRekvestBackAction,
	moveRekvestForwardAction,
	removeRekvestAction
};