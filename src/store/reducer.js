import {
	DOWNLOAD_TESTERS,
	ADD_TESTER,
	ADD_REKVEST,
	EDIT_REKVEST,
	MOVE_REKVEST_BACK,
	MOVE_REKVEST_FORWARD,
	REMOVE_REKVEST
} from './action';

const initialState = {
	testers: []
};

export default function reducer(state = initialState, { type, payload }) {
	switch(type) {
		case DOWNLOAD_TESTERS:
			return {
				...state,
				testers: payload
			};

		case ADD_TESTER:
			return {
				...state,
				testers: [
					...state.testers,
					{
						testerName: payload,
						rekvestes: []
					}
				]
			};

		case ADD_REKVEST:
			return {
				...state,
				testers: state.testers.map(
					(tester, index) => index !== payload.testerId
						? { ...tester }
						: { ...tester, rekvestes: [...tester.rekvestes, payload.rekvestName] }
				)
			};

		case EDIT_REKVEST:
			return {
				...state,
				testers: state.testers.map(
					(tester, index) => index !== payload.testerId
						? { ...tester }
						: {
							...tester,
							rekvestes: tester.rekvestes.map(
								(rekvest, rekvestIndex) => rekvestIndex === payload.rekvestId
									? payload.newRekvestName
									: rekvest
							)
						}
				)
			};

		case MOVE_REKVEST_BACK:
			if (payload.testerId === 0) return state;
			const movedBackRekvest = state.testers[payload.testerId].rekvestes[payload.rekvestId];
			const backRekvestes = state.testers[payload.testerId].rekvestes.filter(
				rekvest => rekvest !== movedBackRekvest
			);
			return {
				...state,
				testers: state.testers.map((tester, index) => {
					if (index === payload.testerId - 1) {  
						return {
							...tester,
							rekvestes: [...tester.rekvestes, movedBackRekvest]
						};
					}

					if (index === payload.testerId) {
						return {
							...tester,
							rekvestes: backRekvestes  
						};
					}

					return { ...tester };
				})
			};

		case MOVE_REKVEST_FORWARD:
			if (payload.testerId === state.testers.lenght - 1) return state;
			const movedForwardRekvest = state.testers[payload.testerId].rekvestes[payload.rekvestId];
			const forwardRekvestes = state.testers[payload.testerId].rekvestes.filter(
				rekvest => rekvest !== movedForwardRekvest
			);
			return {
				...state,
				testers: state.testers.map((tester, index) => {
					if (index === payload.testerId + 1) {  
						return {
							...tester,
							rekvestes: [...tester.rekvestes, movedForwardRekvest]
						};
					}

					if (index === payload.testerId) {
						return {
							...tester,
							rekvestes: forwardRekvestes
						};
					}

					return { ...tester };
				})
			};

		case REMOVE_REKVEST:
			const removedRekvest = state.testers[payload.testerId].rekvestes[payload.rekvestId];
			const rekvestes = state.testers[payload.testerId].rekvestes.filter(
				rekvest => rekvest !== removedRekvest
			);
			return {
				...state,
				testers: state.testers.map(
					(tester, index) => index === payload.testerId
						? {
								...tester,
								rekvestes
						}
						: { ...tester }
				)
			};

		default:
			return state;
	}

}