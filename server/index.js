const express = require('express');
const app = express();
const { readData, writeData } = require('./utils');

const port = 4321;
const hostname = 'localhost';

let testers = [];

// Middleware для разрешения CORS-запросов
app.use((request, response, next) => {
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
	response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
	next();
});

// Middleware для разрешения CORS-запросов
app.use((request, response, next) => {
	console.log(
		(new Date()).toISOString(),
		request.method,
		request.originalUrl
	);
	next();
});

// Middleware для правильного представления request.body
app.use(express.json());

//-------- Routes - пути, по которым идут запросы ----------

app.options('/*', (request, response) => {
	response.statusCode = 200;
	response.send('OK');
});

app.get('/testers', async (request, response) => {
	testers = await readData();
	response.setHeader('Content-Type', 'application/json');
	response.json(testers);
});

app.post('/testers', async (request, response) => {
	testers.push(request.body);
	await writeData(testers);

	response.setHeader('Content-Type', 'application/json');
	response.status(200).json({
		info: `Tester '${request.body.testerName}' was successfully added`
	});
});

app.post('/testers/:testerId/rekvestes', async (request, response) => {
	const { rekvestName } = request.body;
	const testerId = Number(request.params.testerId);

	testers[testerId].rekvestes.push(rekvestName);
	await writeData(testers);

	response.setHeader('Content-Type', 'application/json');
	response.status(200).json({
		info: `Rekvest '${rekvestName}' was successfully added in tester '${testers[testerId].testerName}'`
	});
});

app.patch('/testers/:testerId/rekvestes/:rekvestId', async (request, response) => {
	const { newRekvestName } = request.body;
	const testerId = Number(request.params.testerId);
	const rekvestId = Number(request.params.rekvestId);

	testers[testerId].rekvestes[rekvestId] = newRekvestName;
	await writeData(testers);

	response.setHeader('Content-Type', 'application/json');
	response.status(200).json({
		info: `Rekvest '${rekvestId}' in '${testers[testerId].testerName}' was successfully renamed`
	});
});

app.delete('/testers/:testerId/rekvestes/:rekvestId', async (request, response) => {
	const testerId = Number(request.params.testerId);
	const rekvestId = Number(request.params.rekvestId);

	const removedRekvest = testers[testerId].rekvestes[rekvestId];
	testers[testerId].rekvestes = testers[testerId].rekvestes.filter(
		(rekvest, index) => index !== rekvestId
	);
	await writeData(testers);

	response.setHeader('Content-Type', 'application/json');
	response.status(200).json({
		info: `Rekvest '${removedRekvest}' was successfully removed`
	});
});

app.patch('/testers/:testerId', async (request, response) => {
	const { rekvestId, destTesterId } = request.body;
	const testerId = Number(request.params.testerId);

	if (destTesterId < 0 || destTesterId >= testers.lenght) {
		response.setHeader('Content-Type', 'application/json');
		response.status(400).json({
			error: `Wrong destination tester ID: ${destTesterId}`
		});
	}

	const movedRekvest = testers[testerId].rekvestes[rekvestId];
	testers[testerId].rekvestes = testers[testerId].rekvestes.filter(
		(rekvest, index) => index !== rekvestId
	);
	testers[destTesterId].rekvestes.push(movedRekvest);
	await writeData(testers);

	response.setHeader('Content-Type', 'application/json');
	response.status(200).json({
		info: `Rekvest '${movedRekvest}' was successfully moved`
	});
});

app.listen(port, hostname, (err) => {
	if (err) {
		console.log('Error: ', err);
	}

	console.log(`Server is working on '${hostname}:${port}'`);
});